export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const FACEBOOK_API_VERSION = 'v4.0';
export const API_FACEBOOK = `https://graph.facebook.com/${FACEBOOK_API_VERSION}/`;
// chat_app
export const FACEBOOK_API_ID = '872433833149887';

// test chat app
// export const FACEBOOK_API_ID = '2477062165675063';

export const CONTENT_TYPE = {
  'Content-Type': 'application/json',
};
export const UNITS = [
  {
    max: 2760000,
    value: 60000,
    name: 'phút',
    past: '1 phút trước',
  },
  {
    max: 72000000,
    value: 3600000,
    name: 'giờ',
    past: '1 giờ trước',
  },
];
export const CONVERT_DAY = date => {
  let day = '';
  switch (new Date(date).getDay()) {
    case 0:
      day = 'Chủ nhật';
      break;
    case 1:
      day = 'Thứ hai';
      break;
    case 2:
      day = 'Thứ ba';
      break;
    case 3:
      day = 'Thứ tư';
      break;
    case 4:
      day = 'Thứ năm';
      break;
    case 5:
      day = 'Thứ sáu';
      break;
    case 6:
      day = 'Thứ bảy';
      break;
    default:
  }
  return day;
};
export const TIME_YESTERDAY = 72000000;
export const TIME_WEEK = 518400000;
export const TIME_AGO = date => Math.abs(Date.now() - new Date(date).getTime());
export const TOKEN_USER = localStorage.getItem('token');
export const TOKEN_PAGE = localStorage.getItem('tokenPage');
export const NAME_PAGE = localStorage.getItem('namePage');
export const ID_PAGE = localStorage.getItem('idPage');
