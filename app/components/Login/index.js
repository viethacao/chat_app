import React from 'react';
import logo from './image/logo-login.png';
import './index.css';

const Login = () => (
  <div className="login-container fixed">
    <div className="login-bg-image">
      <div className="login-wrap absolute center">
        <img className="login-thumbnail" src={logo} alt="ogo" />
        <div className="login-content">
          <h2 className="title h2">Chat App</h2>
          <p className="text">Đăng nhập của Quản trị Page.</p>
          <div
            className="fb-login-button"
            data-width=""
            data-size="large"
            data-auto-logout-link="false"
            data-use-continue-as="false"
            data-scope="public_profile,manage_pages,pages_show_list,publish_pages"
            data-onlogin="checkLoginState();"
          />
        </div>
      </div>
    </div>
  </div>
);

export default Login;
