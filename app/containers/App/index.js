/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LoginPage from 'containers/LoginPage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import PageList from 'containers/PageList/Loadable';
import PageChat from 'containers/PageChat/Loadable';
// import { ID_PAGE } from '../../utils/constants';

export default function App() {
  return (
    <Switch>
      <Route exact path="/" component={LoginPage} />
      <Route path="/features" component={FeaturePage} />
      <Route path="/page_list" component={PageList} />
      <Route path="/page_chat" component={PageChat} />
      <Route path="" component={NotFoundPage} />
    </Switch>
  );
}
