export const TOKEN_USER = 'boilerplate/PageList/TOKEN_USER';

export const GET_INFO_USER_PAGE = 'boilerplate/PageList/GET_INFO_USER_PAGE';

export const DELETE_PAGE = 'boilerplate/PageList/DELETE_PAGE';

export const REDIRECT_PAGE = 'boilerplate/PageList/REDIRECT_PAGE';

export const LOG_OUT = 'boilerplate/PageList/LOG_OUT';

export const GET_SEARCH_VALUE = 'boilerplate/PageList/GET_SEARCH_VALUE';

export const GET_SEARCH_PAGE = 'boilerplate/PageList/GET_SEARCH_PAGE';

export const LOAD_MORE = 'boilerplate/PageList/LOAD_MORE';

export const SHOW_MORE_PAGE = 'boilerplate/PageList/SHOW_MORE_PAGE';

export const IS_PAGE_LOADING = 'boilerplate/PageList/IS_PAGE_LOADING';

export const IS_PAGE_SEARCH_LOADING =
  'boilerplate/PageList/IS_PAGE_SEARCH_LOADING';
export const ERROR = 'boilerplate/PageList/ERROR';
