import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import { debounce } from 'lodash';
import RingLoader from 'react-spinners/RingLoader';
import {
  IoIosLogOut,
  IoIosTrash,
  IoIosArrowForward,
  IoIosSearch,
  IoMdFingerPrint,
} from 'react-icons/io';
import {
  ID_PAGE,
  FACEBOOK_API_VERSION,
  FACEBOOK_API_ID,
} from '../../utils/constants';

import {
  makeUserPageInfo,
  makeStatusRedirect,
  getListSearchPage,
  getSearchValueInput,
  getMorePage,
  pageIsLoading,
  pageSearchIsLoading,
} from './selectors';
import {
  deletePage,
  redirectPage,
  getSearchValue,
  getUserPage,
  loadMore,
} from './actions';
import logoBot from './image/logo-bot.png';
import './style.css';

import reducer from './reducer';
import saga from './saga';
export function PageList({
  userPageInfo,
  onDelete,
  onRedirectPage,
  onLogOut,
  onSearchPage,
  userPage,
  listSeachPage,
  searchValue,
  onLoadMore,
  morePage,
  isPageLoad,
  isPageSearchLoad,
}) {
  const key = 'PageList';
  let lists;
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  let listPage;
  if (searchValue !== '') {
    listPage = listSeachPage;
  } else {
    listPage = userPageInfo.accounts;
  }
  if (userPageInfo.accounts && listPage.data) {
    lists = listPage.data.map((item, index) => (
      <div key={item.id} className="sm-col sm-col-12 md-col-4 lg-col-3 pl3 pr3">
        <div className="list-page ">
          <div className="page mb2 pb4 clearfix">
            <img
              src={item.picture.data.url}
              alt="page"
              className="avatar circle left ml2 mt1"
            />
            <div className="name-page left ">{item.name}</div>
          </div>
          <div className="manage-page mt3 ">
            <button
              type="button"
              onClick={onDelete}
              className="btn-delete left ml2 relative"
              data-key={index}
            >
              <IoIosTrash className="relative icon-delete" />
            </button>
            <Link
              to={`/page_chat/${ID_PAGE}`}
              className="btn-submit-page right mr2 text-decoration-none center"
              onClick={onRedirectPage}
              data-token={item.access_token}
              data-name={item.name}
              data-id={item.id}
            >
              Truy Cập
              <IoIosArrowForward />
            </Link>
          </div>
        </div>
      </div>
    ));
  } else {
    lists = <div className="not-page pl3">Bạn không có trang nào!!!!</div>;
  }

  useEffect(() => {
    userPage();
  }, []);

  const pageLoading = isPageLoad ? (
    <div className="loading">
      <RingLoader sizeUnit="px" size={40} color="#4ca842" />
    </div>
  ) : (
    <button
      type="button"
      onClick={onLoadMore}
      className="load-more-button center block"
    >
      Tải thêm trang
    </button>
  );

  const showPageButton = morePage ? pageLoading : '';

  const loadMoreButton =
    searchValue === '' ? (
      showPageButton
    ) : (
      <span className="header">
        Tìm được {listPage.data ? listPage.data.length : '0'} trang
      </span>
    );

  const searchPageLoading = isPageSearchLoad ? (
    <div className="loading-search">
      <RingLoader sizeUnit="px" size={20} color="#4ca842" />
    </div>
  ) : (
    <IoIosSearch className="icon-search absolute " />
  );

  if (!localStorage.getItem('token')) {
    return window.location.assign('/');
  }

  return (
    <div className="wp-pagelist">
      <div className="wrap fit pt1">
        <div className="header m0">
          <img src={logoBot} width="44px" height="44px" alt="logoicon" />
          <div className="user right relative">
            <div className="user__search left m1">
              {searchPageLoading}
              <input
                type="text"
                className="input-search rounded relative"
                placeholder="Tìm Page..."
                onKeyUp={onSearchPage}
              />
            </div>
            <div className="user__info left ml4">
              <img
                src={userPageInfo.picture ? userPageInfo.picture.data.url : ''}
                alt="avatar"
                className="avatar circle"
              />
              <div className="menu-user overflow-hidden absolute p1 rounded left-align">
                <div className="infor-user">
                  <img
                    src={
                      userPageInfo.picture ? userPageInfo.picture.data.url : ''
                    }
                    alt="avatar"
                    className="avatar circle left m2"
                  />
                  <div className="details-infor-user left ml2">
                    <p className="user-name">{userPageInfo.name}</p>
                    <p className="user-id">
                      ID Tài khoản:{' '}
                      <span className="id">{userPageInfo.id}</span>
                    </p>
                  </div>
                </div>
                <IoMdFingerPrint className="icon-finger absolute right" />
                <div className="logout-user ">
                  <button
                    type="button"
                    className="btn-logout mt2 center absolute"
                    onClick={onLogOut}
                  >
                    <IoIosLogOut />
                    Thoát
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="content-box">
          <h2 className="title pl3">CHỌN PAGE BẮT ĐẦU </h2>
        </div>
        <div className=" clearfix wrap-page">{lists}</div>
        {loadMoreButton}
      </div>
    </div>
  );
}
PageList.propTypes = {
  onDelete: PropTypes.func,
  userPageInfo: PropTypes.object,
  onRedirectPage: PropTypes.func,
  onSearchPage: PropTypes.func,
  onLogOut: PropTypes.func,
  userPage: PropTypes.func,
  listSeachPage: PropTypes.object,
  searchValue: PropTypes.string,
  onLoadMore: PropTypes.func,
  morePage: PropTypes.string,
  isPageLoad: PropTypes.bool,
  isPageSearchLoad: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  userPageInfo: makeUserPageInfo(),
  statusRedirect: makeStatusRedirect(),
  listSeachPage: getListSearchPage(),
  searchValue: getSearchValueInput(),
  morePage: getMorePage(),
  isPageLoad: pageIsLoading(),
  isPageSearchLoad: pageSearchIsLoading(),
});
export function mapDispatchToProps(dispatch) {
  const dispatchDebounce = debounce(dispatch, 1000);
  return {
    onDelete: e => dispatch(deletePage(e.target.dataset.key)),
    onRedirectPage: e => {
      dispatch(
        redirectPage(
          e.target.dataset.token,
          e.target.dataset.name,
          e.target.dataset.id,
        ),
      );
    },
    onLogOut: () => {
      ((d, s, id) => {
        const fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        const js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');

      window.fbAsyncInit = () => {
        window.FB.init({
          appId: FACEBOOK_API_ID,
          cookie: true,
          xfbml: true,
          version: FACEBOOK_API_VERSION,
        });
      };

      window.FB.getLoginStatus(() => {
        window.FB.logout();
        localStorage.removeItem('token');
        localStorage.removeItem('tokenPage');
        window.location.assign('/');
      });
    },
    onSearchPage: event => {
      dispatchDebounce(getSearchValue(event.target.value));
    },
    userPage: () => {
      dispatch(getUserPage());
    },
    onLoadMore: () => {
      dispatch(loadMore());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PageList);
