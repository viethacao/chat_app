import {
  GET_INFO_USER_PAGE,
  DELETE_PAGE,
  REDIRECT_PAGE,
  GET_SEARCH_VALUE,
  TOKEN_USER,
  GET_SEARCH_PAGE,
  LOAD_MORE,
  SHOW_MORE_PAGE,
  IS_PAGE_LOADING,
  IS_PAGE_SEARCH_LOADING,
  ERROR,
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} username The new text of the input field
 *
 * @return {object} An action object with a type of CHANGE_USERNAME
 */
export function getUserPage() {
  return {
    type: TOKEN_USER,
  };
}

export function getUserPageInfoSuccess(userPageInfo) {
  return {
    type: GET_INFO_USER_PAGE,
    userPageInfo,
  };
}

export function deletePage(item) {
  return {
    type: DELETE_PAGE,
    item,
  };
}
export function redirectPage(token, name, id) {
  return {
    type: REDIRECT_PAGE,
    token,
    name,
    id,
  };
}
export function getSearchValue(searchValueInput) {
  return {
    type: GET_SEARCH_VALUE,
    searchValueInput,
  };
}

export function searchPageSuccess(listPage, seachVal) {
  return {
    type: GET_SEARCH_PAGE,
    listPage,
    seachVal,
  };
}

export function loadMore() {
  return {
    type: LOAD_MORE,
  };
}

export function showMorePage(morePage) {
  return {
    type: SHOW_MORE_PAGE,
    morePage,
  };
}

export function isPageLoading(isPageLoad) {
  return {
    type: IS_PAGE_LOADING,
    isPageLoad,
  };
}

export function isPageSearchLoading(isPageSearchLoad) {
  return {
    type: IS_PAGE_SEARCH_LOADING,
    isPageSearchLoad,
  };
}
export function repoLoadError(error) {
  return {
    type: ERROR,
    error,
  };
}
