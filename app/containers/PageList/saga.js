/**
 * Gets the repositories of the user from Github
 */

import { put, call, select, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';
import { repoLoadingError } from 'containers/App/actions';
import { getSearchValueInput, getMorePage } from './selectors';
import {
  getUserPageInfoSuccess,
  searchPageSuccess,
  showMorePage,
  isPageLoading,
  isPageSearchLoading,
  repoLoadError,
} from './actions';
import { TOKEN_USER, GET_SEARCH_VALUE, LOAD_MORE } from './constants';
import { API_FACEBOOK } from '../../utils/constants';

/**
 * Github repos request/response handler
 */
export function* getUserPageInfos() {
  // Select username from store
  const token = yield localStorage.getItem('token');
  const requestURL = `${API_FACEBOOK}me?fields=id,name,picture{url},accounts.limit(4){name,picture{url},access_token}&access_token=${token}`;

  try {
    // Call our request helper (see 'utils/request')
    const userPageRes = yield call(request, requestURL);
    yield put(getUserPageInfoSuccess(userPageRes));
  } catch (err) {
    yield put(repoLoadError(err));
  }
}

export function* getPageSearch() {
  // Select username from store
  const token = yield localStorage.getItem('token');
  const searchValue = yield select(getSearchValueInput());

  const requestURL = `${API_FACEBOOK}me?fields=accounts{name,picture{url},access_token}&access_token=${token}`;

  try {
    // Call our request helper (see 'utils/request')
    yield put(isPageSearchLoading(true));
    const listPage = yield call(request, requestURL);
    yield put(searchPageSuccess(listPage, searchValue));
  } catch (err) {
    yield put(repoLoadingError(err));
  } finally {
    yield put(isPageSearchLoading(false));
  }
}

export function* loadMore() {
  // Select username from store
  const requestURL = yield select(getMorePage());

  try {
    // Call our request helper (see 'utils/request')
    yield put(isPageLoading(true));
    const morePage = yield call(request, requestURL);
    yield put(showMorePage(morePage));
  } catch (err) {
    yield put(repoLoadingError(err));
  } finally {
    yield put(isPageLoading(false));
  }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  yield takeLatest(TOKEN_USER, getUserPageInfos);
  yield takeLatest(GET_SEARCH_VALUE, getPageSearch);
  yield takeLatest(LOAD_MORE, loadMore);
}
