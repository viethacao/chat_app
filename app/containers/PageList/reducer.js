/* eslint-disable prettier/prettier */

import produce from 'immer';
import {
  GET_INFO_USER_PAGE,
  DELETE_PAGE,
  REDIRECT_PAGE,
  GET_SEARCH_VALUE,
  GET_SEARCH_PAGE,
  SHOW_MORE_PAGE,
  IS_PAGE_LOADING,
  IS_PAGE_SEARCH_LOADING,
  ERROR
} from './constants';
// The initial state of the App
export const initialState = {
  userPageInfo: {},
  statusRedirect: false,
  isLogOut: false,
  searchListPage:{},
  searchValue: '',
  loadmore:'',
  isPageLoad:false,
  isPageSearchLoad:false
};

/* eslint-disable default-case, no-param-reassign */

const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_INFO_USER_PAGE:
        draft.userPageInfo = action.userPageInfo;
        draft.loadmore = action.userPageInfo.accounts.paging.next;
        break;

      case DELETE_PAGE:
        if(draft.searchValue === '')
          delete draft.userPageInfo.accounts.data[action.item];
        else {
          delete draft.searchListPage.data[action.item];
          delete draft.userPageInfo.accounts.data[action.item];
        }
        break;

      case REDIRECT_PAGE:  draft.statusRedirect = true;
        localStorage.setItem('tokenPage', action.token );
        localStorage.setItem('namePage', action.name );
        localStorage.setItem('idPage', action.id );
        
        break;
      case GET_SEARCH_VALUE:
        draft.searchValue = action.searchValueInput;
        break;
    
      case GET_SEARCH_PAGE: {
        const filterTemp = action.listPage.accounts;
        if (action.seachVal !== '') {
            
          const arrFilter = filterTemp.data;
          const newList = arrFilter.filter(item =>
            item.name
              .toLowerCase()
              .includes(action.seachVal.toLowerCase()),
          );
          draft.searchListPage.data = newList;            
        } else {
          draft.searchListPage = draft.userPageInfo.accounts;
        }
      }
        break;
  
      case SHOW_MORE_PAGE: 
        if(draft.userPageInfo.accounts.data){
          action.morePage.data.forEach(item =>{
            draft.userPageInfo.accounts.data.push(item)
          })
        }
        draft.loadmore = action.morePage.paging.next
        break;

      case IS_PAGE_LOADING: 
        draft.isPageLoad = action.isPageLoad
        break;

      case IS_PAGE_SEARCH_LOADING: 
        draft.isPageSearchLoad = action.isPageSearchLoad
        break;
      case ERROR: 
        localStorage.removeItem('token')
        localStorage.removeItem('tokenPage')
        localStorage.removeItem('namePage')
        draft.userPageInfo = {}
        break;
    }
  });

export default homeReducer;
