/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.PageList || initialState;
const makeUserPageInfo = () =>
  createSelector(
    selectLogin,
    loginState => loginState.userPageInfo,
  );
const makeStatusRedirect = () =>
  createSelector(
    selectLogin,
    loginState => loginState.statusRedirect,
  );

const getTokenPage = () =>
  createSelector(
    selectLogin,
    loginState => loginState.tokenPage,
  );

const getSearchValueInput = () =>
  createSelector(
    selectLogin,
    loginState => loginState.searchValue,
  );

const getListSearchPage = () =>
  createSelector(
    selectLogin,
    loginState => loginState.searchListPage,
  );

const getMorePage = () =>
  createSelector(
    selectLogin,
    loginState => loginState.loadmore,
  );

const pageIsLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.isPageLoad,
  );

const pageSearchIsLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.isPageSearchLoad,
  );

export {
  selectLogin,
  makeUserPageInfo,
  makeStatusRedirect,
  getTokenPage,
  getSearchValueInput,
  getListSearchPage,
  getMorePage,
  pageIsLoading,
  pageSearchIsLoading,
};
