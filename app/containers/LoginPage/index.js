/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/injectReducer';
import Login from 'components/Login';
import { FACEBOOK_API_VERSION, FACEBOOK_API_ID } from '../../utils/constants';
import { getTokenUser } from './actions';
import { makeTokenUser } from './selectors';
import reducer from './reducer';

const key = 'login';

export function LoginPage({ statusChangeCallback }) {
  useInjectReducer({ key, reducer });
  useEffect(() => {
    ((d, s, id) => {
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      const js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');

    window.fbAsyncInit = () => {
      window.FB.init({
        appId: FACEBOOK_API_ID,
        cookie: true,
        xfbml: true,
        version: FACEBOOK_API_VERSION,
      });

      window.FB.getLoginStatus(response => {
        statusChangeCallback(response);
      });
    };

    window.checkLoginState = () => {
      window.FB.getLoginStatus(response => {
        statusChangeCallback(response);
      });
    };
  }, []);
  return localStorage.getItem('token') ? (
    <Redirect to="/page_list" />
  ) : (
    <Login />
  );
}

const mapStateToProps = createStructuredSelector({
  tokenUser: makeTokenUser(),
});

LoginPage.propTypes = {
  statusChangeCallback: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    statusChangeCallback: response => {
      if (response.status === 'connected') {
        dispatch(getTokenUser(response.authResponse.accessToken));
      }
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoginPage);
