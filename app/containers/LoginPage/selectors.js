/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.login || initialState;

const makeTokenUser = () =>
  createSelector(
    selectLogin,
    loginState => loginState.token,
  );
export { selectLogin, makeTokenUser };
