/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable indent */
import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import {
  IoIosSend,
  IoIosArrowBack,
  IoIosMail,
  IoIosDownload,
} from 'react-icons/io';
import { FaPhone, FaBirthdayCake, FaAddressCard } from 'react-icons/fa';

import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import './style.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import RingLoader from 'react-spinners/RingLoader';
// import PulseLoader from 'react-spinners/PulseLoader';
// import LoginPage from 'containers/LoginPage/Loadable';
// import PageList from 'containers/PageList/Loadable';

import {
  getConversation,
  getMessage,
  getLinkLoadMessage,
  valuePostMessage,
  postMessage,
  linkLoadUserAction,
} from './actions';
import {
  getConversationSelector,
  getIdConversationSelector,
  isUserLoading,
  isMessageLoading,
  isConversationLoading,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  UNITS,
  CONVERT_DAY,
  TIME_YESTERDAY,
  TIME_AGO,
  TIME_WEEK,
  // TOKEN_USER,
  // TOKEN_PAGE,
  // NAME_PAGE,
} from '../../utils/constants';
function PageChat({
  onShowPage,
  conversation,
  onGetMessages,
  idConversation,
  onLoadMessage,
  getValuePostMessage,
  onLoadUser,
  onEnterPostMessage,
  isUserLoad,
  // isMessageLoad,
  onSendMessage,
  // isConversationLoad,
}) {
  const key = 'PageChat';
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useEffect(() => {
    onShowPage();
  }, []);

  const format = (diff, divisor, unit, past) => {
    const val = Math.round(Math.abs(diff) / divisor);
    return val <= 1 ? past : `${val} ${unit} trước`;
  };

  const ago = date => {
    const diff = Date.now() - date.getTime();
    if (Math.abs(diff) < 60000) return 'vừa xong';
    for (let i = 0; i < UNITS.length; i += 1) {
      if (Math.abs(diff) < UNITS[i].max) {
        return format(diff, UNITS[i].value, UNITS[i].name, UNITS[i].past);
      }
    }
    return format(diff, 31536000000, 'năm', 'năm trước');
  };

  const agoChat = date => {
    if (TIME_AGO(date) >= TIME_WEEK) {
      return `${new Date(date).getDate()}/${new Date(date).getMonth() +
        1}/${`${`${new Date(date).getFullYear()} ${new Date(
        date,
      ).getHours()}:`}${new Date(date).getMinutes()}`}`;
    }
    if (TIME_AGO(date) >= TIME_YESTERDAY) {
      return `${CONVERT_DAY(date)} ${new Date(date).getHours()}:${new Date(
        date,
      ).getMinutes()}`;
    }
    return `${new Date(date).getHours()}:${new Date(date).getMinutes()}`;
  };

  const agoUser = date =>
    TIME_AGO(date) >= TIME_YESTERDAY
      ? `${new Date(date).getDate()}/${new Date(date).getMonth() + 1}`
      : ago(new Date(date));

  let getUser;
  let elementMessage = [];
  let linkLoadUser;

  const activeId = item => {
    let elementSticker;
    if (idConversation === item) {
      elementSticker = 'active';
    }
    return elementSticker;
  };

  if (conversation.data) {
    if (conversation.paging) {
      linkLoadUser = conversation.paging.next;
    }
    elementMessage = conversation.data.filter(
      item => idConversation === item.id,
    );

    if (conversation.data.length === 0) {
      getUser = (
        <p className="none-conver center">Không có cuộc trò chuyện nào</p>
      );
    } else {
      getUser = conversation.data.map(item => (
        <div
          role="presentation"
          key={item.id}
          onClick={onGetMessages}
          className={`item ${activeId(item.id)}`}
          data-id={item.id}
          data-name={nameUser}
        >
          <img className="item-image" src={item.image} alt="" />
          <div className="info-item">
            <span className="name-item">{item.participants.data[0].name}</span>
            <span className="time-item right">
              {agoUser(new Date(item.updated_time))}
            </span>
          </div>
          <p className="message-latest overflow-hidden">{item.snippet}</p>
        </div>
      ));
    }
  }

  let nameUser;
  let imageUser;
  let mainMessage;
  let infoUser;
  let emailUser;
  if (elementMessage[0]) {
    nameUser = elementMessage[0].participants.data[0].name;
    if (elementMessage[0].image) {
      imageUser = (
        <img
          className="image-user-header"
          src={elementMessage[0].image}
          alt="1"
        />
      );
    }

    emailUser = elementMessage[0].participants.data[0].email;
    infoUser = (
      <div>
        {imageUser}
        <span className="user-conver inline-block">{nameUser}</span>
        <div>
          <p className="about">Thông tin</p>
          <p>
            <FaPhone className="icon-info" />
            <span className="email-user">+ Số điện thoại</span>
          </p>
          <p>
            <IoIosMail className="icon-info" />
            <span className="email-user">{emailUser}</span>
          </p>
          <p>
            <FaBirthdayCake className="icon-info" />
            <span className="email-user">+ Ngày sinh</span>
          </p>
          <p>
            <FaAddressCard className="icon-info" />
            <span className="email-user">+ Liên hệ</span>
          </p>
        </div>
      </div>
    );
    const sticker = item => {
      let elementSticker;
      if (item) {
        elementSticker = <img className="sticker" src={item} alt="" />;
      }
      return elementSticker;
    };

    const attachments = item => {
      let elementSticker;
      if (item) {
        item.data.map(ele =>
          ele.file_url
            ? (elementSticker = (
                <a href={ele.file_url} keys={ele.id}>
                  <IoIosDownload className="icon-download" />
                  {ele.name}
                  <br />
                </a>
              ))
            : (elementSticker = (
                <img className="gif" src={ele.image_data.url} alt="img" />
              )),
        );
      }
      return elementSticker;
    };

    const activeSend = item => {
      let classActive;
      if (item) {
        classActive = 'active-send';
      }
      return classActive;
    };

    if (elementMessage[0].dataMessage) {
      mainMessage = (
        <div
          className="conversation flex overflow-auto"
          id={elementMessage[0].id}
          onScroll={onLoadMessage}
          data-link={elementMessage[0].dataMessage.messages.paging.next}
        >
          {elementMessage[0].dataMessage.messages.data.map(item =>
            item.to.data[0].name ===
            conversation.data[0].participants.data[1].name ? (
              <div className="user-left relative main-user" key={item.id}>
                <div className="content-mess relative content-mess-left inline-block">
                  <span className="inline-block bg-white message">
                    {attachments(item.attachments)}
                    {sticker(item.sticker)}
                    {item.message}
                  </span>
                  <span className="time time-left absolute">
                    {agoChat(new Date(item.created_time))}
                  </span>
                </div>
              </div>
            ) : (
              <div className="user-right right main-user" key={item.id}>
                <div className="content-mess relative content-mess-right right">
                  <span className="inline-block message message-right">
                    {attachments(item.attachments)}
                    {sticker(item.sticker)}
                    {item.message}
                  </span>
                  <span className="time time-right absolute">
                    {agoChat(new Date(item.created_time))}
                  </span>
                </div>
              </div>
            ),
          )}

          <form className="post-message absolute">
            <textarea
              onKeyDown={onEnterPostMessage}
              data-id={idConversation}
              onChange={getValuePostMessage}
              className="input-message"
              placeholder="Gửi tin nhắn..."
              value={elementMessage[0].value}
            />
            <button
              data-id={idConversation}
              type="button"
              className={`btn-send absolute ${activeSend(
                elementMessage[0].value,
              )}`}
              onClick={onSendMessage}
              data-input={elementMessage[0].value}
            >
              <IoIosSend />
            </button>
          </form>
        </div>
      );
    }
  }

  const isLoadingUser = isUserLoad ? (
    <div className="loading-user flex absolute">
      <RingLoader sizeUnit="px" size={30} color="#4ca842" />
    </div>
  ) : (
    <button
      onClick={onLoadUser}
      type="button"
      className="btn-loadmore block relative overflow-hidden"
      data-link={linkLoadUser}
    >
      Tải thêm
    </button>
  );

  let loadUser;
  if (linkLoadUser) {
    loadUser = isLoadingUser;
  }
  // let isLoadingMessage;
  // if (isMessageLoad) {
  //   isLoadingMessage = (
  //     <div className="loading-message flex absolute">
  //       <PulseLoader sizeUnit="px" size={10} color="#4ca842" />
  //     </div>
  //   );
  // }

  // if (!TOKEN_USER) {
  //   return <Redirect to="/" component={LoginPage} />;
  // }

  // if (!TOKEN_USER && !TOKEN_PAGE) {
  //   return <Redirect to="/page_list" component={PageList} />;
  // }

  return (
    <div>
      <div className="header-chat bg-blue relative">
        <div className="header-left inline-block">
          <Link to="/page_list" className="btn-back" role="presentation">
            <IoIosArrowBack />
          </Link>
          <span className="name-user">{localStorage.getItem('namePage')}</span>
        </div>
        {imageUser}
        <span className="user-conver inline-block">{nameUser}</span>
      </div>
      <div className="content relative flex">
        <div className="sidebar-left overflow-auto bg-white inline-block relative overflow-auto">
          {getUser}
          {loadUser}
        </div>
        <div className="sidebar-right relative">
          {mainMessage}
          {/* {isLoadingMessage}
          {isConversationLoad ? (
            <div className="loading-conversation flex">
              <PulseLoader sizeUnit="px" size={20} color="#4ca842" />
            </div>
          ) : (
            mainMessage
          )} */}
        </div>
        <div className="sidebar-user">{infoUser}</div>
      </div>
    </div>
  );
}

PageChat.propTypes = {
  onShowPage: PropTypes.func,
  conversation: PropTypes.object,
  onGetMessages: PropTypes.func,
  idConversation: PropTypes.string,
  onLoadMessage: PropTypes.func,
  getValuePostMessage: PropTypes.func,
  onLoadUser: PropTypes.func,
  onEnterPostMessage: PropTypes.func,
  isUserLoad: PropTypes.bool,
  // isMessageLoad: PropTypes.bool,
  onSendMessage: PropTypes.func,
  // isConversationLoad: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  conversation: getConversationSelector(),
  idConversation: getIdConversationSelector(),
  isUserLoad: isUserLoading(),
  isMessageLoad: isMessageLoading(),
  isConversationLoad: isConversationLoading(),
});

export function mapDispatchToProps(dispatch) {
  const arrValuePost = [];

  return {
    onShowPage: () => {
      dispatch(getConversation());
    },

    onGetMessages: e => {
      // dispatch(getMessage(e.currentTarget.dataset.id));
      localStorage.setItem('idConversation', e.currentTarget.dataset.id);
      setInterval(() => {
        dispatch(getMessage(localStorage.getItem('idConversation')));
      }, 2000);
    },

    getValuePostMessage: e => {
      dispatch(
        valuePostMessage(e.currentTarget.dataset.id, e.currentTarget.value),
      );
    },

    onEnterPostMessage: e => {
      if (
        ((e.shiftKey && e.key === 'Enter') || e.keyCode === 32) &&
        e.currentTarget.value === ''
      ) {
        e.preventDefault();
      }
      if (!e.shiftKey && e.key === 'Enter') {
        e.preventDefault();
        if (e.currentTarget.value) {
          arrValuePost.unshift(e.currentTarget.value);
          dispatch(
            postMessage(e.currentTarget.dataset.id, e.currentTarget.value),
          );
        }
      }
    },

    onLoadUser: e => {
      e.preventDefault();
      dispatch(linkLoadUserAction(e.currentTarget.dataset.link));
    },

    onLoadMessage: e => {
      if (e.currentTarget.scrollTop === 0 && e.currentTarget.dataset.link) {
        dispatch(getLinkLoadMessage(e.currentTarget.dataset.link));
      }
    },

    onSendMessage: e => {
      if (e.currentTarget.dataset.input) {
        arrValuePost.unshift(e.currentTarget.dataset.input);
        dispatch(
          postMessage(
            e.currentTarget.dataset.id,
            e.currentTarget.dataset.input,
          ),
        );
      }
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PageChat);
