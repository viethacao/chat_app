/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.PageChat || initialState;

const getConversationSelector = () =>
  createSelector(
    selectLogin,
    loginState => loginState.conversation,
  );
const getIdConversationSelector = () =>
  createSelector(
    selectLogin,
    loginState => loginState.idConversation,
  );
const linkLoadMessageSelector = () =>
  createSelector(
    selectLogin,
    loginState => loginState.linkLoadMessage,
  );
const postMessageSelector = () =>
  createSelector(
    selectLogin,
    loginState => loginState.valuePostMessage,
  );
const linkLoadUserSelector = () =>
  createSelector(
    selectLogin,
    loginState => loginState.linkLoadUser,
  );
const isUserLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.isUserLoad,
  );
const isMessageLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.isMessageLoad,
  );
const isConversationLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.isConversationLoad,
  );
const getIdUser = () =>
  createSelector(
    selectLogin,
    loginState => loginState.idUser,
  );

export {
  selectLogin,
  getConversationSelector,
  getIdConversationSelector,
  linkLoadMessageSelector,
  postMessageSelector,
  linkLoadUserSelector,
  isUserLoading,
  isMessageLoading,
  getIdUser,
  isConversationLoading,
};
