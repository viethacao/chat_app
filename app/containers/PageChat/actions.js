/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  GET_CONVERSATION,
  GET_CONVERSATION_SUCCESS,
  GET_LINK_LOAD_MESSAGE,
  GET_MESSAGE_LOAD_SUCCESS,
  GET_MESSAGE,
  VALUE_POST_MESSAGE,
  POST_MESSAGE,
  LINK_LOAD_USER,
  LINK_LOAD_USER_SUCCESS,
  IS_USER_LOADING,
  IS_MESSAGE_LOADING,
  IMAGE_USER,
  IMAGE_USER_SUCCESS,
  GET_MESSAGE_SUCCESS,
  POST_MESSAGE_SUCCESS,
  ERROR,
  IMAGE_USER_LOAD,
  IS_CONVERSATION_LOADING,
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} username The new text of the input field
 *
 * @return {object} An action object with a type of CHANGE_USERNAME
 */
export function getConversation() {
  return {
    type: GET_CONVERSATION,
  };
}

export function getConversationSuccess(response) {
  return {
    type: GET_CONVERSATION_SUCCESS,
    response,
  };
}

export function getMessage(idConversation) {
  return {
    type: GET_MESSAGE,
    idConversation,
  };
}

export function getLinkLoadMessage(link) {
  return {
    type: GET_LINK_LOAD_MESSAGE,
    link,
  };
}

export function getMessageLoadSuccess(response) {
  return {
    type: GET_MESSAGE_LOAD_SUCCESS,
    response,
  };
}

export function valuePostMessage(id, value) {
  return {
    type: VALUE_POST_MESSAGE,
    id,
    value,
  };
}

export function postMessage(id, value) {
  return {
    type: POST_MESSAGE,
    id,
    value,
  };
}

export function linkLoadUserAction(link) {
  return {
    type: LINK_LOAD_USER,
    link,
  };
}

export function linkLoadUserSuccess(response) {
  return {
    type: LINK_LOAD_USER_SUCCESS,
    response,
  };
}

export function isUserLoading(isUserLoad) {
  return {
    type: IS_USER_LOADING,
    isUserLoad,
  };
}

export function isMessageLoading(isMessageLoad) {
  return {
    type: IS_MESSAGE_LOADING,
    isMessageLoad,
  };
}

export function isConversationLoading(isConversationLoad) {
  return {
    type: IS_CONVERSATION_LOADING,
    isConversationLoad,
  };
}

export function getImageUser(response) {
  return {
    type: IMAGE_USER,
    response,
  };
}
export function getImageUserSuccess(response) {
  return {
    type: IMAGE_USER_SUCCESS,
    response,
  };
}
export function getMessageSuccessAction(response) {
  return {
    type: GET_MESSAGE_SUCCESS,
    response,
  };
}
export function postMessageSuccess(response) {
  return {
    type: POST_MESSAGE_SUCCESS,
    response,
  };
}
export function loadError(error) {
  return {
    type: ERROR,
    error,
  };
}
export function imageUserLoad(response) {
  return {
    type: IMAGE_USER_LOAD,
    response,
  };
}
