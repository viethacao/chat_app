export const GET_CONVERSATION = 'chatApp/PageChat/GET_CONVERSATION';
export const GET_CONVERSATION_SUCCESS =
  'boilerplate/PageChat/GET_CONVERSATION_SUCCESS';
export const GET_MESSAGE = 'chatApp/PageChat/GET_MESSAGE';
export const GET_MESSAGE_SUCCESS = 'chatApp/PageChat/GET_MESSAGE_SUCCESS';
export const GET_LINK_LOAD_MESSAGE = 'chatApp/PageChat/GET_LINK_LOAD_MESSAGE';
export const GET_MESSAGE_LOAD_SUCCESS =
  'chatApp/PageChat/GET_MESSAGE_LOAD_SUCCESS';
export const VALUE_POST_MESSAGE = 'chatApp/PageChat/VALUE_POST_MESSAGE';
export const POST_MESSAGE = 'chatApp/PageChat/POST_MESSAGE';
export const POST_MESSAGE_SUCCESS = 'chatApp/PageChat/POST_MESSAGE_SUCCESS';
export const LINK_LOAD_USER = 'chatApp/PageChat/LINK_LOAD_USER';
export const LINK_LOAD_USER_SUCCESS = 'chatApp/PageChat/LINK_LOAD_USER_SUCCESS';
export const IS_USER_LOADING = 'chatApp/PageChat/IS_USER_LOADING';
export const IS_MESSAGE_LOADING = 'chatApp/PageChat/IS_MESSAGE_LOADING';
export const IMAGE_USER = 'chatApp/PageChat/IMAGE_USER';
export const IMAGE_USER_SUCCESS = 'chatApp/PageChat/IMAGE_USER_SUCCESS';
export const ERROR = 'chatApp/PageChat/ERROR';
export const IMAGE_USER_LOAD = 'chatApp/PageChat/IMAGE_USER_LOAD';
export const IS_CONVERSATION_LOADING =
  'chatApp/PageChat/IS_CONVERSATION_LOADING';
