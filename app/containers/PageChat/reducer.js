/* eslint-disable prettier/prettier */
/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  GET_CONVERSATION_SUCCESS, 
  GET_LINK_LOAD_MESSAGE, 
  GET_MESSAGE_LOAD_SUCCESS, 
  GET_MESSAGE, 
  VALUE_POST_MESSAGE, 
  POST_MESSAGE, 
  LINK_LOAD_USER, 
  LINK_LOAD_USER_SUCCESS,
  IS_USER_LOADING,
  IS_MESSAGE_LOADING,
  IMAGE_USER_SUCCESS,
  GET_MESSAGE_SUCCESS,
  POST_MESSAGE_SUCCESS,
  IS_CONVERSATION_LOADING,
  ERROR, 
} from './constants';

// The initial state of the App
export const initialState = {
  conversation: {},
  idConversation: '',
  linkLoadMessage: '',
  valuePostMessage: {},
  linkLoadUser:'',
  isUserLoad:false,
  isMessageLoad:false,
  isConversationLoad:false,
  idUser: []
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_CONVERSATION_SUCCESS:
        draft.conversation = action.response
        draft.conversation.data.forEach(item => {
          item.value = '';
          item.name = item.participants.data[0].name
        })
        break;

      case GET_MESSAGE:
        draft.idConversation = action.idConversation;
        break;
        
      case GET_LINK_LOAD_MESSAGE:
        draft.linkLoadMessage = action.link;
        break;

      case GET_MESSAGE_LOAD_SUCCESS: {
        const getConversation = draft.conversation.data.filter(item => draft.idConversation === item.id);
        action.response.data.forEach(element => {
          getConversation[0].dataMessage.messages.data.push(element)
        });
        getConversation[0].dataMessage.messages.paging.next = action.response.paging.next
      }
        break;

      case VALUE_POST_MESSAGE: {
        const getConversation = draft.conversation.data.filter(item => action.id === item.id);
        getConversation[0].value = action.value;
      }
        break;

      case POST_MESSAGE: {
        const item = {
          id: action.id,
          value: action.value,
        };
        draft.valuePostMessage = item;
        const getConversation = draft.conversation.data.filter(element => action.id === element.id);
        getConversation[0].value = '';
      } 
        break;

      case LINK_LOAD_USER: 
        draft.linkLoadUser = action.link
        break;

      case LINK_LOAD_USER_SUCCESS: 
        action.response.data.forEach(item => {
          draft.idUser.push(item.participants.data[0].id)
        })
        action.response.data.forEach(item => {
          draft.conversation.data.push(item)
          item.value = '';
          item.name = item.participants.data[0].name
        })
        draft.conversation.paging.next = action.response.paging.next;
        break;

      case IS_USER_LOADING: 
        draft.isUserLoad = action.isUserLoad
        break;

      case IS_MESSAGE_LOADING: 
        draft.isMessageLoad = action.isMessageLoad
        break;

      case IS_CONVERSATION_LOADING: 
        draft.isConversationLoad = action.isConversationLoad
        break;

      case IMAGE_USER_SUCCESS:

        draft.conversation.data.forEach((item) => {
          action.response.forEach( items => {
            if(items.id === item.participants.data[0].id){
              item.image = items.profile_pic;
            }
          })
        })
        break;

      case GET_MESSAGE_SUCCESS: {
        const conversation = draft.conversation.data.filter(item => item.id === action.response.id)
        conversation[0].dataMessage = action.response
      }
        break;

      case POST_MESSAGE_SUCCESS: {
        const conversation = draft.conversation.data.filter(item => item.id === action.response.id);
        const item = {
          "to": {
            "data": [
              {
                "name": '',
                "email": '',
                "id": ''
              },
              {
                "name": conversation[0].participants.data[1].name,
                "email": '',
                "id": ''
              }
            ]
          },
          "message": action.response.value.trim(), 
          "created_time": new Date,
          "id": ''
        }
        conversation[0].dataMessage.messages.data.unshift(item)
      }
        break;
      case ERROR: 
        localStorage.removeItem('token')
        localStorage.removeItem('tokenPage')
        localStorage.removeItem('namePage')
        draft.conversation = {}
        break;
    }
  });

export default homeReducer;
