/**
 * Gets the repositories of the user from Github
 */

import { put, call, select, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';
import { repoLoadingError } from 'containers/App/actions';
import {
  getConversationSuccess,
  getMessageLoadSuccess,
  linkLoadUserSuccess,
  isUserLoading,
  isMessageLoading,
  getImageUser,
  getImageUserSuccess,
  getMessageSuccessAction,
  postMessageSuccess,
  loadError,
  imageUserLoad,
  isConversationLoading,
} from './actions';

import {
  GET_CONVERSATION,
  GET_LINK_LOAD_MESSAGE,
  POST_MESSAGE,
  LINK_LOAD_USER,
  IMAGE_USER,
  GET_MESSAGE,
  IMAGE_USER_LOAD,
} from './constants';
import { API_FACEBOOK, CONTENT_TYPE } from '../../utils/constants';

import {
  linkLoadMessageSelector,
  postMessageSelector,
  linkLoadUserSelector,
  getConversationSelector,
  getIdConversationSelector,
  getIdUser,
} from './selectors';
/**
 * Github repos request/response handler
 */

export function* getConversationSaga() {
  const tokenPage = localStorage.getItem('tokenPage');
  const requestURL = `${API_FACEBOOK}me/conversations?fields=participants,snippet,updated_time&limit=2&access_token=${tokenPage}`;

  try {
    const response = yield call(request, requestURL);
    yield put(getConversationSuccess(response));
    yield put(getImageUser());
  } catch (err) {
    yield put(loadError(err));
  }
}

export function* getMessageSaga() {
  const id = yield select(getIdConversationSelector());
  const tokenPage = localStorage.getItem('tokenPage');
  const requestURL = `${API_FACEBOOK}${id}?fields=messages{to,message,created_time,sticker,attachments}&access_token=${tokenPage}`;
  try {
    yield put(isConversationLoading(true));
    const response = yield call(request, requestURL);
    yield put(getMessageSuccessAction(response));
  } catch (err) {
    yield put(repoLoadingError(err));
  } finally {
    yield put(isConversationLoading(false));
  }
}

export function* getImageUserSaga() {
  const tokenPage = localStorage.getItem('tokenPage');
  const conversation = yield select(getConversationSelector());
  try {
    const arrUser = [];
    const arrImage = [];
    conversation.data.forEach(item => {
      arrUser.push({
        method: 'GET',
        relative_url: item.participants.data[0].id,
      });
    });
    const requestURL = `${API_FACEBOOK}?include_headers=false`;
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: CONTENT_TYPE,
      body: JSON.stringify({
        access_token: tokenPage,
        batch: arrUser,
      }),
    });

    response.forEach(item => {
      arrImage.push(JSON.parse(item.body));
    });

    yield put(getImageUserSuccess(arrImage));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

export function* getMoreMessageSaga() {
  const url = yield select(linkLoadMessageSelector());
  try {
    yield put(isMessageLoading(true));
    const response = yield call(request, url);
    yield put(getMessageLoadSuccess(response));
  } catch (err) {
    yield put(repoLoadingError(err));
  } finally {
    yield put(isMessageLoading(false));
  }
}

export function* postMessageSaga() {
  const tokenPage = yield localStorage.getItem('tokenPage');
  const valuePostMessage = yield select(postMessageSelector());
  const requestURL = `${API_FACEBOOK}${
    valuePostMessage.id
  }/messages?access_token=${tokenPage}`;
  try {
    yield call(request, requestURL, {
      method: 'POST',
      headers: CONTENT_TYPE,
      body: JSON.stringify({
        message: valuePostMessage.value,
      }),
    });
    yield put(postMessageSuccess(valuePostMessage));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

export function* getMoreUserSaga() {
  const url = yield select(linkLoadUserSelector());
  try {
    yield put(isUserLoading(true));
    const response = yield call(request, url);
    yield put(linkLoadUserSuccess(response));
    yield put(imageUserLoad(response));
  } catch (err) {
    yield put(repoLoadingError(err));
  } finally {
    yield put(isUserLoading(false));
  }
}

export function* getIdUserLoadSaga() {
  const tokenPage = localStorage.getItem('tokenPage');
  const conversation = yield select(getIdUser());

  try {
    const arrUser = [];
    const arrImage = [];
    conversation.forEach(item => {
      arrUser.push({
        method: 'GET',
        relative_url: item,
      });
    });
    const requestURL = `${API_FACEBOOK}?include_headers=false`;
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: CONTENT_TYPE,
      body: JSON.stringify({
        access_token: tokenPage,
        batch: arrUser,
      }),
    });

    response.forEach(item => {
      arrImage.push(JSON.parse(item.body));
    });

    yield put(getImageUserSuccess(arrImage));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* watcher() {
  yield takeLatest(GET_CONVERSATION, getConversationSaga);
  yield takeLatest(GET_MESSAGE, getMessageSaga);
  yield takeLatest(LINK_LOAD_USER, getMoreUserSaga);
  yield takeLatest(GET_LINK_LOAD_MESSAGE, getMoreMessageSaga);
  yield takeLatest(IMAGE_USER, getImageUserSaga);
  yield takeLatest(POST_MESSAGE, postMessageSaga);
  yield takeLatest(IMAGE_USER_LOAD, getIdUserLoadSaga);
}
