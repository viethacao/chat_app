import {
  getConversationSelector,
  getIdConversationSelector,
  linkLoadMessageSelector,
  postMessageSelector,
  linkLoadUserSelector,
  isMessageLoading,
  isUserLoading,
} from '../selectors';
describe('getConversationSelector', () => {
  const getConversation = getConversationSelector();
  it('getConversationSelector', () => {
    const conversation = {};
    const mockedState = {
      home: {
        conversation,
      },
    };
    expect(getConversation(mockedState)).toEqual(conversation);
  });
});
describe('getIdConversationSelector', () => {
  const getMessage = getIdConversationSelector();
  it('getConversationSelector', () => {
    const idConversation = '';
    const mockedState = {
      home: {
        idConversation,
      },
    };
    expect(getMessage(mockedState)).toEqual(idConversation);
  });
});
describe('linkLoadMessageSelector', () => {
  const getLink = linkLoadMessageSelector();
  it('getConversationSelector', () => {
    const link = '';
    const mockedState = {
      home: {
        link,
      },
    };
    expect(getLink(mockedState)).toEqual(link);
  });
});
describe('postMessageSelector', () => {
  const postMessage = postMessageSelector();
  it('getConversationSelector', () => {
    const value = {};
    const mockedState = {
      home: {
        value,
      },
    };
    expect(postMessage(mockedState)).toEqual(value);
  });
});
describe('linkLoadUserSelector', () => {
  const getLink = linkLoadUserSelector();
  it('linkLoadUserSelector', () => {
    const link = '';
    const mockedState = {
      home: {
        link,
      },
    };
    expect(getLink(mockedState)).toEqual(link);
  });
});
describe('isUserLoading', () => {
  const getLink = isUserLoading();
  it('isUserLoading', () => {
    const isUserLoad = false;
    const mockedState = {
      home: {
        isUserLoad,
      },
    };
    expect(getLink(mockedState)).toEqual(isUserLoad);
  });
});
describe('isMessageLoading', () => {
  const getLink = isMessageLoading();
  it('isMessageLoading', () => {
    const isMessageLoad = false;
    const mockedState = {
      home: {
        isMessageLoad,
      },
    };
    expect(getLink(mockedState)).toEqual(isMessageLoad);
  });
});
