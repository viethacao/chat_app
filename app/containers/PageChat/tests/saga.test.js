/**
 * Tests for HomePage sagas
 */

import { put, takeLatest } from 'redux-saga/effects';
import { repoLoadingError } from 'containers/App/actions';
import {
  getConversationSuccess,
  getImageUser,
  getMessageSuccessAction,
  isUserLoading,
  isMessageLoading,
  getMessageLoadSuccess,
  linkLoadUserSuccess,
  // repoLoadError,
  getImageUserSuccess,
  imageUserLoad,
} from '../actions';
import {
  GET_CONVERSATION,
  GET_MESSAGE,
  LINK_LOAD_USER,
  GET_LINK_LOAD_MESSAGE,
  IMAGE_USER,
  // POST_MESSAGE,
} from '../constants';
import githubData, {
  getConversationSaga,
  getMessageSaga,
  getLinkLoadUserSaga,
  getLinkLoadMessageSaga,
  getImageUserSaga,
  // postMessageSaga,
} from '../saga';

/* eslint-disable redux-saga/yield-effects */
describe('Saga', () => {
  let getConversationGenerator;
  let getMessageGeneration;
  let getLinkLoadUserGenerator;
  let getLinkMessageGenerator;
  let getImageGenerator;

  beforeEach(() => {
    getConversationGenerator = getConversationSaga();
    getMessageGeneration = getMessageSaga();
    getLinkLoadUserGenerator = getLinkLoadUserSaga();
    getLinkMessageGenerator = getLinkLoadMessageSaga();
    getImageGenerator = getImageUserSaga();

    const conversationDescriptor = getConversationGenerator.next().value;
    expect(conversationDescriptor).toMatchSnapshot();

    const messageDescriptor = getMessageGeneration.next().value;
    expect(messageDescriptor).toMatchSnapshot();
    const callmessageDescriptor = getMessageGeneration.next().value;
    expect(callmessageDescriptor).toMatchSnapshot();

    const linkUserDescriptor = getLinkLoadUserGenerator.next().value;
    expect(linkUserDescriptor).toMatchSnapshot();

    const linkMessageDescriptor = getLinkMessageGenerator.next().value;
    expect(linkMessageDescriptor).toMatchSnapshot();

    const imageDescriptor = getImageGenerator.next().value;
    expect(imageDescriptor).toMatchSnapshot();
    const callImageDescriptor = getImageGenerator.next().value;
    expect(callImageDescriptor).toMatchSnapshot();
  });

  it('should dispatch the reposLoaded, respon action if it requests the data successfully', () => {
    const response = {};
    const putConversationDescriptor = getConversationGenerator.next(response)
      .value;
    const putImageDescriptor = getConversationGenerator.next().value;
    expect(putConversationDescriptor).toEqual(
      put(getConversationSuccess(response)),
    );
    expect(putImageDescriptor).toEqual(put(getImageUser()));

    const putMessageDescriptor = getMessageGeneration.next(response).value;
    expect(putMessageDescriptor).toEqual(
      put(getMessageSuccessAction(response)),
    );

    const putImageUserDescriptor = getImageGenerator.next(response).value;
    expect(putImageUserDescriptor).toEqual(put(getImageUserSuccess(response)));

    const putIsUserDescriptor = getLinkLoadUserGenerator.next(true).value;
    expect(putIsUserDescriptor).toEqual(put(isUserLoading(true)));
    const callLinkUserDescriptor = getLinkLoadUserGenerator.next().value;
    expect(callLinkUserDescriptor).toMatchSnapshot();
    const putloadUserDescriptor = getLinkLoadUserGenerator.next(response).value;
    expect(putloadUserDescriptor).toEqual(put(linkLoadUserSuccess(response)));
    const putImageMessageDescriptor = getLinkLoadUserGenerator.next().value;
    expect(putImageMessageDescriptor).toEqual(put(imageUserLoad(response)));
    const putIsUser2Descriptor = getLinkLoadUserGenerator.next(false).value;
    expect(putIsUser2Descriptor).toEqual(put(isUserLoading(false)));

    const putIsMessageDescriptor = getLinkMessageGenerator.next(true).value;
    expect(putIsMessageDescriptor).toEqual(put(isMessageLoading(true)));
    const callLinkMessageDescriptor = getLinkMessageGenerator.next().value;
    expect(callLinkMessageDescriptor).toMatchSnapshot();
    const putMessageLoadDescriptor = getLinkMessageGenerator.next(response)
      .value;
    expect(putMessageLoadDescriptor).toEqual(
      put(getMessageLoadSuccess(response)),
    );
  });

  it('should call the repoLoadingError action if the response errors', () => {
    const response = new Error('Some error');
    const putMessageDescriptor = getMessageGeneration.throw(response).value;
    expect(putMessageDescriptor).toEqual(put(repoLoadingError(response)));
  });
});
describe('githubDataSaga Saga', () => {
  const githubDataSaga = githubData();

  it('should start task to watch for action', () => {
    const takeLatestDescriptor = githubDataSaga.next().value;
    const takeLatestMessageDescriptor = githubDataSaga.next().value;
    const takeLatestUserDescriptor = githubDataSaga.next().value;
    const takeLatestLoadMessageDescriptor = githubDataSaga.next().value;
    const takeLatestImageDescriptor = githubDataSaga.next().value;

    expect(takeLatestDescriptor).toEqual(
      takeLatest(GET_CONVERSATION, getConversationSaga),
    );

    expect(takeLatestMessageDescriptor).toEqual(
      takeLatest(GET_MESSAGE, getMessageSaga),
    );

    expect(takeLatestUserDescriptor).toEqual(
      takeLatest(LINK_LOAD_USER, getLinkLoadUserSaga),
    );

    expect(takeLatestLoadMessageDescriptor).toEqual(
      takeLatest(GET_LINK_LOAD_MESSAGE, getLinkLoadMessageSaga),
    );

    expect(takeLatestImageDescriptor).toEqual(
      takeLatest(IMAGE_USER, getImageUserSaga),
    );
  });
});
