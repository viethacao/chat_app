import produce from 'immer';
import homeReducer from '../reducer';
import {
  getConversationSuccess,
  getMessage,
  getLinkLoadMessage,
  getMessageLoadSuccess,
  valuePostMessage,
  postMessage,
  linkLoadUserAction,
  linkLoadUserSuccess,
  getImageUserSuccess,
  getMessageSuccessAction,
  postMessageSuccess,
  isUserLoading,
  isMessageLoading,
  repoLoadError,
} from '../actions';
/* eslint-disable default-case, no-param-reassign */
describe('homeReducer', () => {
  let state;
  beforeEach(() => {
    state = {
      conversation: {},
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
      isUserLoad: false,
      isMessageLoad: false,
      idUser: [],
    };
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(homeReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the getMessage action correctly', () => {
    const fixture = '';
    const expectedResult = produce(state, draft => {
      draft.idConversation = fixture;
    });

    expect(homeReducer(state, getMessage(fixture))).toEqual(expectedResult);
  });
  it('should handle the getLinkLoadMessage action correctly', () => {
    const fixture = '';
    const expectedResult = produce(state, draft => {
      draft.linkLoadMessage = fixture;
    });

    expect(homeReducer(state, getLinkLoadMessage(fixture))).toEqual(
      expectedResult,
    );
  });
  it('should handle the linkLoadUserAction action correctly', () => {
    const fixture = '';
    const expectedResult = produce(state, draft => {
      draft.linkLoadUser = fixture;
    });

    expect(homeReducer(state, linkLoadUserAction(fixture))).toEqual(
      expectedResult,
    );
  });
  it('should handle the postMessage action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          response: {},
          name: 'hihi',
          value: 'haha',
        },
      ],
    };
    state = {
      conversation: fixture,
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };
    const id = 0;
    const value = 'hihi';
    const item = {
      id,
      value,
    };
    const expectedResult = produce(state, draft => {
      draft.valuePostMessage = item;
      const getConversation = draft.conversation.data.filter(
        element => id === element.id,
      );
      getConversation[0].value = '';
    });
    expect(homeReducer(state, postMessage(id, value))).toEqual(expectedResult);
  });

  it('should handle the getConversationSuccess action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          response: {},
          participants: {
            data: [{ name: 'hihi' }],
          },
        },
      ],
    };

    const expectedResult = produce(state, draft => {
      draft.conversation = fixture;
      draft.conversation.data.forEach(item => {
        item.value = '';
        item.name = item.participants.data[0].name;
      });
    });
    expect(homeReducer(state, getConversationSuccess(fixture))).toEqual(
      expectedResult,
    );
  });

  it('should handle the valuePostMessage action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          response: {},
          name: 'hihi',
          value: 'haha',
        },
      ],
    };
    state = {
      conversation: fixture,
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };
    const id = 0;
    const value = 'hihi';
    const expectedResult = produce(state, draft => {
      const getConversation = draft.conversation.data.filter(
        item => id === item.id,
      );
      getConversation[0].value = value;
    });

    expect(homeReducer(state, valuePostMessage(id, value))).toEqual(
      expectedResult,
    );
  });

  it('should handle the linkLoadUserSuccess action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          response: {},
          name: 'hihi',
          value: 'haha',
        },
      ],
      paging: {
        next: 'http://',
      },
    };
    state = {
      conversation: fixture,
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
      idUser: [],
    };
    const response = {
      data: [
        {
          id: 0,
          participants: {
            data: [
              {
                name: 'haha',
              },
            ],
          },
        },
      ],
      paging: {
        next: 'http://123',
      },
    };
    const expectedResult = produce(state, draft => {
      response.data.forEach(item => {
        draft.idUser.push(item.participants.data[0].id);
      });
      response.data.forEach(item => {
        draft.conversation.data.push(item);
        item.value = '';
        item.name = item.participants.data[0].name;
      });
      draft.conversation.paging.next = response.paging.next;
    });

    expect(homeReducer(state, linkLoadUserSuccess(response))).toEqual(
      expectedResult,
    );
  });
  it('should handle the getMessageLoadSuccess action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          dataMessage: {
            messages: {
              data: [],
              paging: {
                next: 'http://123',
              },
            },
          },
        },
      ],
    };
    const response = {
      data: [
        {
          message: '123',
        },
      ],
      paging: {
        next: 'http://123',
      },
    };
    state = {
      conversation: fixture,
      idConversation: 0,
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };
    const expectedResult = produce(state, draft => {
      const getConversation = draft.conversation.data.filter(
        item => draft.idConversation === item.id,
      );
      response.data.forEach(element => {
        getConversation[0].dataMessage.messages.data.push(element);
      });
      getConversation[0].dataMessage.messages.paging.next =
        response.paging.next;
    });

    expect(homeReducer(state, getMessageLoadSuccess(response))).toEqual(
      expectedResult,
    );
  });

  it('should handle the isUserLoading action correctly', () => {
    const fixture = true;
    const expectedResult = produce(state, draft => {
      draft.isUserLoad = fixture;
    });

    expect(homeReducer(state, isUserLoading(fixture))).toEqual(expectedResult);
  });

  it('should handle the isMessageLoading action correctly', () => {
    const fixture = true;
    const expectedResult = produce(state, draft => {
      draft.isMessageLoad = fixture;
    });

    expect(homeReducer(state, isMessageLoading(fixture))).toEqual(
      expectedResult,
    );
  });

  it('should handle the getMessageSuccessAction action correctly', () => {
    const response = {
      id: 0,
    };
    const fixture = {
      data: [
        {
          id: 0,
          dataMessage: {},
        },
      ],
    };
    state = {
      conversation: fixture,
      idConversation: 0,
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };

    const expectedResult = produce(state, draft => {
      const conversation = draft.conversation.data.filter(
        item => item.id === response.id,
      );
      conversation[0].dataMessage = response;
    });

    expect(homeReducer(state, getMessageSuccessAction(response))).toEqual(
      expectedResult,
    );
  });

  it('should handle the repoLoadError action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          response: {},
          name: 'hihi',
          value: 'haha',
        },
      ],
    };
    state = {
      conversation: fixture,
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };
    const error = {};
    const expectedResult = produce(state, draft => {
      draft.conversation = {};
    });

    expect(homeReducer(state, repoLoadError(error))).toEqual(expectedResult);
  });

  it('should handle the postMessageSuccess action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          participants: {
            data: [
              {
                name: '1',
              },
              {
                name: '2',
              },
            ],
          },
          response: {},
          name: 'hihi',
          value: 'haha',
          dataMessage: {
            messages: {
              data: [],
            },
          },
        },
      ],
    };
    const response = {
      id: 0,
      value: 'haha',
    };
    state = {
      conversation: fixture,
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };
    const expectedResult = produce(state, draft => {
      const conver = draft.conversation.data.filter(
        item => item.id === response.id,
      );
      const item = {
        to: {
          data: [
            {
              name: '',
              email: '',
              id: '',
            },
            {
              name: conver[0].participants.data[1].name,
              email: '',
              id: '',
            },
          ],
        },
        message: response.value,
        created_time: new Date(),
        id: '',
      };
      conver[0].dataMessage.messages.data.unshift(item);
    });

    expect(homeReducer(state, postMessageSuccess(response))).toEqual(
      expectedResult,
    );
  });

  it('should handle the getImageUserSuccess action correctly', () => {
    const fixture = {
      data: [
        {
          id: 0,
          response: {},
          name: 'hihi',
          value: 'haha',
          participants: {
            data: [{ id: 0 }, { id: 1 }],
          },
        },
      ],
    };
    state = {
      conversation: fixture,
      idConversation: '',
      linkLoadMessage: '',
      valuePostMessage: {},
      linkLoadUser: '',
    };
    const response = [
      {
        id: 0,
        profile_pic: 'http://123',
      },
      {
        id: 1,
        profile_pic: 'http://1234',
      },
    ];

    const expectedResult = produce(state, draft => {
      draft.conversation.data.forEach(item => {
        response.forEach(items => {
          if (items.id === item.participants.data[0].id) {
            item.image = items.profile_pic;
          }
        });
      });
    });

    expect(homeReducer(state, getImageUserSuccess(response))).toEqual(
      expectedResult,
    );
  });
});
