import {
  GET_CONVERSATION,
  GET_CONVERSATION_SUCCESS,
  GET_LINK_LOAD_MESSAGE,
  GET_MESSAGE_LOAD_SUCCESS,
  GET_MESSAGE,
  VALUE_POST_MESSAGE,
  POST_MESSAGE,
  LINK_LOAD_USER,
  LINK_LOAD_USER_SUCCESS,
  IS_USER_LOADING,
  IS_MESSAGE_LOADING,
  IMAGE_USER,
  IMAGE_USER_SUCCESS,
  GET_MESSAGE_SUCCESS,
  POST_MESSAGE_SUCCESS,
  ERROR,
} from '../constants';

import {
  getConversation,
  getConversationSuccess,
  getMessage,
  getLinkLoadMessage,
  getMessageLoadSuccess,
  valuePostMessage,
  postMessage,
  linkLoadUserAction,
  linkLoadUserSuccess,
  isUserLoading,
  isMessageLoading,
  getImageUser,
  getImageUserSuccess,
  getMessageSuccessAction,
  postMessageSuccess,
  repoLoadError,
} from '../actions';
describe('getConversation', () => {
  it('getConversation', () => {
    const expectedResult = {
      type: GET_CONVERSATION,
    };

    expect(getConversation()).toEqual(expectedResult);
  });
});
describe('getConversationSuccess', () => {
  it('getConversationSuccess', () => {
    const response = {};
    const expectedResult = {
      type: GET_CONVERSATION_SUCCESS,
      response,
    };

    expect(getConversationSuccess(response)).toEqual(expectedResult);
  });
});
describe('getMessage', () => {
  it('getMessage', () => {
    const idConversation = '';
    const expectedResult = {
      type: GET_MESSAGE,
      idConversation,
    };

    expect(getMessage(idConversation)).toEqual(expectedResult);
  });
});
describe('getLinkLoadMessage', () => {
  it('getLinkLoadMessage', () => {
    const link = '';
    const expectedResult = {
      type: GET_LINK_LOAD_MESSAGE,
      link,
    };

    expect(getLinkLoadMessage(link)).toEqual(expectedResult);
  });
});
describe('getMessageLoadSuccess', () => {
  it('getMessageLoadSuccess', () => {
    const response = '';
    const expectedResult = {
      type: GET_MESSAGE_LOAD_SUCCESS,
      response,
    };

    expect(getMessageLoadSuccess(response)).toEqual(expectedResult);
  });
});
describe('valuePostMessage', () => {
  it('valuePostMessage', () => {
    const id = 123;
    const value = 'hihi';
    const expectedResult = {
      type: VALUE_POST_MESSAGE,
      id,
      value,
    };

    expect(valuePostMessage(id, value)).toEqual(expectedResult);
  });
});
describe('postMessage', () => {
  it('postMessage', () => {
    const id = 123;
    const value = 'hihi';
    const expectedResult = {
      type: POST_MESSAGE,
      id,
      value,
    };

    expect(postMessage(id, value)).toEqual(expectedResult);
  });
});
describe('linkLoadUserAction', () => {
  it('linkLoadUserAction', () => {
    const link = '';
    const expectedResult = {
      type: LINK_LOAD_USER,
      link,
    };

    expect(linkLoadUserAction(link)).toEqual(expectedResult);
  });
});
describe('linkLoadUserSuccess', () => {
  it('linkLoadUserSuccess', () => {
    const response = {};
    const expectedResult = {
      type: LINK_LOAD_USER_SUCCESS,
      response,
    };

    expect(linkLoadUserSuccess(response)).toEqual(expectedResult);
  });
});
describe('isUserLoading', () => {
  it('isUserLoading', () => {
    const isUserLoad = false;
    const expectedResult = {
      type: IS_USER_LOADING,
      isUserLoad,
    };

    expect(isUserLoading(isUserLoad)).toEqual(expectedResult);
  });
});
describe('isMessageLoading', () => {
  it('isMessageLoading', () => {
    const isMessageLoad = false;
    const expectedResult = {
      type: IS_MESSAGE_LOADING,
      isMessageLoad,
    };

    expect(isMessageLoading(isMessageLoad)).toEqual(expectedResult);
  });
});
describe('getImageUser', () => {
  it('getImageUser', () => {
    const response = {};
    const expectedResult = {
      type: IMAGE_USER,
      response,
    };

    expect(getImageUser(response)).toEqual(expectedResult);
  });
});
describe('getImageUserSuccess', () => {
  it('getImageUserSuccess', () => {
    const response = {};
    const expectedResult = {
      type: IMAGE_USER_SUCCESS,
      response,
    };

    expect(getImageUserSuccess(response)).toEqual(expectedResult);
  });
});
describe('getMessageSuccessAction', () => {
  it('getMessageSuccessAction', () => {
    const response = {};
    const expectedResult = {
      type: GET_MESSAGE_SUCCESS,
      response,
    };

    expect(getMessageSuccessAction(response)).toEqual(expectedResult);
  });
});
describe('postMessageSuccess', () => {
  it('postMessageSuccess', () => {
    const response = {};
    const expectedResult = {
      type: POST_MESSAGE_SUCCESS,
      response,
    };

    expect(postMessageSuccess(response)).toEqual(expectedResult);
  });
});
describe('repoLoadError', () => {
  it('repoLoadError', () => {
    const error = {};
    const expectedResult = {
      type: ERROR,
      error,
    };

    expect(repoLoadError(error)).toEqual(expectedResult);
  });
});
